package clasesTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import clasesPrincipales.CP01_Validación_Menú_Principal;
import clasesPrincipales.CP02_Agregar_producto_al_carrito;
import clasesPrincipales.conexionChrome;

import org.testng.annotations.BeforeMethod;

public class TestValidaciónMenuPrincipal {
conexionChrome cd=new conexionChrome();
WebDriver wd;

@BeforeMethod
public void setUp() {
  wd=cd.ConexionChrome();
}

@Test(priority = 0)
public void CP01_Validación_Menú_Principal() {
	CP01_Validación_Menú_Principal mp=new CP01_Validación_Menú_Principal(wd);
    mp.ValidaciónMenuPrincipal();
}
@Test(priority = 1)
public void CP02_Agregar_producto_al_carrito_() {
	CP02_Agregar_producto_al_carrito ac=new CP02_Agregar_producto_al_carrito(wd);
ac.CP02_Agregar_producto_al_carrito();
}

@AfterMethod
public void tearDown() {
	wd.quit();
}

}
